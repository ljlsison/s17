console.log("Hello World")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// Activity 1
function userInfo(){
	let userName = prompt("What is your name?")
	let userAge = prompt("How old are you?")
	let userAddress = prompt("Where do you live?")

	console.log("Hello " + userName);
	console.log("You are " + userAge + " years old.")
	console.log("You live in " + userAddress)	
}

userInfo();

// Activity 2
function bandFavorites(){
	let bandFirst = "Parokya ni Edgar";
	let bandSecond = "Eraserheads";
	let bandThird = "Maroon 5";
	let bandFourth = "Bruno Mars";
	let bandFifth = "Post Malone";

	console.log("1. " + bandFirst);
	console.log("2. " + bandSecond);
	console.log("3. " + bandThird);
	console.log("4. " + bandFourth);
	console.log("5. " + bandFifth);
}

bandFavorites();


// Activity 3
function movieFavorites(){
	let movieFirstTitle = "Kung Fu Panda 1"
	let movieFirstRating = "87%"

	let movieSecondTitle = "Kung Fu Panda 2"
	let movieSecondRating = "80%"

	let movieThirdTitle = "Kung Fu Panda 3"
	let movieThirdRating = "86%"

	let movieFourthTitle = "Kung Fu Hustle"
	let movieFourthRating = "90%"

	let movieFifthTitle = "Despicable Me"
	let movieFifthRating = "81%"

	console.log("1. " + movieFirstTitle);
	console.log("Rotten Tomatoes Rating: " + movieFirstRating);

	console.log("2. " + movieSecondTitle);
	console.log("Rotten Tomatoes Rating: " + movieSecondRating);

	console.log("3. " + movieThirdTitle);
	console.log("Rotten Tomatoes Rating: " + movieThirdRating);

	console.log("4. " + movieFourthTitle);
	console.log("Rotten Tomatoes Rating: " + movieFourthRating);

	console.log("5. " + movieFifthTitle);
	console.log("Rotten Tomatoes Rating: " + movieFifthRating);


}

movieFavorites();




// Activity 4

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();

// console.log(friend1);
// console.log(friend2);